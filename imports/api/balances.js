import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Match } from 'meteor/check'

export const Balances = new Mongo.Collection('balances');

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish('balances', function balancesPublication(id){
      return Balances.find({'id_exercise':id});
  });
  Meteor.publish('balances_by_fm', function balancesByFM(id,exercise){
     return  Balances.find({'id_fm': id, 'id_exercise': exercise});
  });
}

Meteor.methods({
  'balances.insert' (balances){
    Match.test(balances, [{ id_investor: Match.Integer, id_project: Match.Integer, id_fm: Match.Integer, amount: Number }]);
    balances.map(function(balance){
      Balances.insert(balance)
    })
    console.log(balances)
    return balances
  }
});
