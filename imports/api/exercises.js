import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Match } from 'meteor/check'
import "./projects.js"
import "./counters.js"

export const Exercises = new Mongo.Collection('exercises');

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish('exercises', function exercisesPublication(){
    return Exercises.find();
  });
}


Meteor.methods({
  'exercises.insert' (exercise){
    if(!this.isSimulation){
      Match.test(exercise, { id: Match.Integer, name: String });
      var counter = Meteor.call('counters.insert', { investors: 1, projects: 0, id_exercise: exercise.id })
      var project = Meteor.call('projects.insert', { id: 0, Name: 'Capital', description: 'Cuenta de capital', id_exercise: exercise.id});
      if(project && counter){
        Exercises.insert(exercise)
        return exercise
      }else{
        throw new Meteor.Error(500, 'Internal server error')
      }
    }
  }
});
