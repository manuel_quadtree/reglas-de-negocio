import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { Counters } from './counters.js';
import { Match } from 'meteor/check'

export const Investors = new Mongo.Collection('investors');

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish('investors', function investorsPublication(){
    return Investors.find({});
  });
}

Meteor.methods({
  'investors.insert' (investor){
    if(this.isSimulation){ return }
    Match.test(investor, { Name: String, description: String, active: Boolean, id_exercise: Match.Integer });
    var counter = Meteor.call('counters.update', investor.id_exercise, 'investor')
    if(counter){
      investor.id = counter.investors;
      Investors.insert(investor)
      return investor
    }else{
      throw new Meteor.Error(500, "Couldn't create a new investor")
    }
  },
  'investors.toggle'(investor){
    Investors.update(investor._id,{ $set: { active: !investor.active } });
    return investor
  }
});
