import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Match } from 'meteor/check';

export const Counters = new Mongo.Collection('counters');

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish('counters', function countersPublication(exercise){
    return Counters.find({'id_exercise':exercise});
  });
}

Meteor.methods({
  'counters.update' (id_exercise, parameter){
    var counter = Counters.find({'id_exercise':id_exercise}).fetch()[0]
    console.log(counter)
    switch(parameter){
      case 'investor':
        Counters.update(counter._id, { $set: { 'investors': counter.investors + 1 }})
        break
      case 'project':
        Counters.update(counter._id, { $set: { 'projects': counter.projects + 1 }})
        break
      default:
        throw new Meteor.Error(500, "Couldn't update counter" );
    }
    return counter
  },
  'counters.insert' (counter){
      Match.test(counter, {projects: Match.Integer, investors: Match.Integer, id_exercise: Match.Integer})
      Counters.insert(counter);
      return counter
  }
});
