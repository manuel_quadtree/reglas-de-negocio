import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export const Movements = new Mongo.Collection('movements');

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish('movements', function movementsPublication(){
    return Movements.find();
  });
}
