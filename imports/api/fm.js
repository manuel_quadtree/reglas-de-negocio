import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Match } from 'meteor/check'
import { Balances } from './balances.js'
import { Amounts } from './amounts.js'

export const FM = new Mongo.Collection('fm');

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish('fm', function fmPublication(){
    return FM.find({});
  });
}


Meteor.methods({
  'fm.insert' (fm, amounts, balances){
    Match.test(fm, { id: Match.Integer, createdAt: String, Date: String, id_movement: Match.Integer, id_project: Match.Integer, reverted: Boolean, observation: String });
    var balances_arr = Meteor.call('balances.insert', balances)
    if(balances_arr.length){
      var amounts_arr = Meteor.call('amounts.insert', amounts)
      if(amounts_arr.length){
        FM.insert(fm)
        return fm
      }else{
        Balances.remove({id_fm: fm.id})
        throw new Meteor.Error(500, 'Internal server error')
      }
    }else{
      throw new Meteor.Error(500, 'Internal server error')
    }
  },
  'fm.revert'(fm){
    FM.update(fm._id,{ $set: { reverted: !fm.reverted } });
    return fm
  }
});
