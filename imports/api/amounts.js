import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Match } from 'meteor/check'

export const Amounts = new Mongo.Collection('amounts');

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish('amounts', function amountsPublication(id){
    return Amounts.find({'id_exercise':id});
  });
  Meteor.publish('amounts_by_fm', function amountsByFM(id, exercise){
    return Amounts.find({'id_fm': id, 'id_exercise': exercise });
  });
}

Meteor.methods({
  'amounts.insert' (amounts){
    Match.test(amounts, [{ id_investor: Match.Integer, id_fm: Match.Integer, amount: Number }]);
    amounts.map(function(amount){
      Amounts.insert(amount)
    })
    console.log(amounts)
    return amounts
  }
});
