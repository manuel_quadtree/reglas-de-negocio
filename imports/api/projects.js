import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { Counters } from './counters.js';
import { Match } from 'meteor/check';

export const Projects = new Mongo.Collection('projects');

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish('projects', function projectsPublication(){
    return Projects.find({});
  });
}

Meteor.methods({
  'projects.insert' (project){
    if(!this.isSimulation){
      Match.test(project, { id: Match.Integer, Name: String, description: String, id_exercise: Match.Integer });
      var counter = Meteor.call('counters.update', project.id_exercise, 'project')
      if(counter){
        project.id = counter.projects;
        Projects.insert(project)
        return project
      }else{
        throw new Meter.Error(500, "Couldn't create a new project")
      }
    }
  }
});
