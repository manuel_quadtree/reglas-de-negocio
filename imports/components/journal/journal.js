
import angular       from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter      from 'angular-ui-router';
import template      from './journal.html';
import { Amounts }   from '../../api/amounts.js';
import { Balances }  from '../../api/balances.js';
import { FM }        from '../../api/fm.js';
import { Investors } from '../../api/investors.js';
import { Projects }  from '../../api/projects.js';

class JournalCtrl {
    constructor($scope, $reactive, $stateParams, $filter) {
        'ngInject';
        $reactive(this).attach($scope);
        let ctrl = this;
        console.log("-----------------------------------------------------------")
        let exercise_id = Number($stateParams.exercise_id);
        //call DB for everything
        ctrl.changeFMInfo = function(){
          if(ctrl.selected_fm && ctrl.fm.length){
            var selected_fm = ctrl.fm.find(function(fm){ return fm.id == ctrl.selected_fm })
            ctrl.table_project = selected_fm.id_project.toString();
            ctrl.table_movement = selected_fm.id_movement.toString();
            ctrl.table_date = selected_fm.Date;
            ctrl.table_observation = selected_fm.observation;
          }
        }
        ctrl.selected_fm = 0
        ctrl.SearchFMInfo = function(){
            ctrl.subscribe('amounts_by_fm', function(){return [ctrl.selected_fm, exercise_id]}, function(){
                ctrl.amounts = Amounts.find({'id_fm':ctrl.selected_fm, 'id_exercise':exercise_id}).fetch();
                console.log("received amounts")
            });
            ctrl.subscribe('balances_by_fm', function(){return [ctrl.selected_fm, exercise_id]}, function(){
                ctrl.balances = Balances.find({'id_fm':ctrl.selected_fm, 'id_exercise':exercise_id}).fetch();
                console.log("received balances");
            });
        }
        ctrl.subscribe('investors', function(){
            ctrl.investors = Investors.find({'id_exercise':exercise_id}).fetch();
            console.log("received investors")
        });
        ctrl.subscribe('projects', function(){
            ctrl.projects = Projects.find({'id_exercise':exercise_id}).fetch();
            console.log("received projects")
        });
        ctrl.subscribe('fm', function(){
           ctrl.fm = FM.find({'id_exercise':exercise_id}).fetch();
           ctrl.fm = $filter('orderBy')(ctrl.fm, 'Date')
           console.log(ctrl.fm)
           console.log("received fms")
           ctrl.selected_fm = ctrl.fm.length ? ctrl.fm[ctrl.fm.length - 1].id : 0;
           ctrl.select_fm = ctrl.fm.length ? ctrl.fm[ctrl.fm.length - 1].id : 0;
           ctrl.SearchFMInfo()
           ctrl.changeFMInfo()
        });
        ctrl.setInvestorSum = function(investor){
            if(ctrl.balances){
                var sum = ctrl.balances.filter(function(x){return x.id_investor == investor.id})
                sum = sum.reduce(function(a, b){return (a*100 + (b.amount ? b.amount : 0)*100)/100; }, 0);
                return sum
            }
        }
        ctrl.setInvestorAmount = function(investor){
            if(ctrl.amounts){
                var amount = ctrl.amounts.find(function(x){ return x.id_investor == investor.id });
                return amount ? amount.amount : 0;
            }
        }
        //M rows
        ctrl.setInvestorM = function(investor, project){
            if(ctrl.amounts){
              var amount = ctrl.amounts.find(function(x){ return x.id_investor == investor.id });
              amount = amount ? amount.amount : 0
              var fm = ctrl.fm.find(function(fm){ return fm.id == ctrl.selected_fm });
              if(fm){
                switch (fm.id_movement) {
                  case 1:
                    if (project.id == 0){
                      return amount
                    }else{
                      return 0
                    }
                  case 2:
                    if (project.id == 0){
                      return amount*(-1)
                    }else if(fm.id_project == project.id){
                      return amount
                    }else{
                      return 0
                    }
                  case 3:
                    if(fm.id_project == project.id){
                      return amount
                    }else{
                      return 0
                    }
                  case 4:
                    if (fm.id_project == project.id){
                      return amount*(-1)
                    }else if(fm.id_project == 0){
                      return amount
                    }else{
                      return 0
                    }
                  case 5:
                    if (project.id == 0){
                      return amount*(-1)
                    }else{
                      return 0
                    }
                  default:
                    return "error"
                }
              }
          }
        }
        ctrl.setProjectM = function(project){
          var ms = ctrl.investors.map(function(investor){
            return ctrl.setInvestorM(investor, project)
          })
          return ms.reduce(function(a,b){return a+b}, 0)
        }
        //S rows
        ctrl.setProjectBalance = function(project){
            if(ctrl.balances){
                var balance = ctrl.balances.filter(function(x){return x.id_project == project.id})
                balance = balance.reduce(function(a,b){return a + (b.amount ? b.amount : 0); },0);
                return balance;
            }
        }
        ctrl.setInvestorByProjectAmount = function(investor, project, fm){
            if(ctrl.balances){
                var amount = ctrl.balances.find(function(x){ return x.id_investor == investor.id && x.id_project == project.id && x.id_fm == fm})
                amount = amount ? amount.amount : 0;
                return amount
            }
        }
       ctrl.setProjectTotal = function(project){
          var amounts = ctrl.investors.map(function(investor){
            return ctrl.setInvestorByProjectAmount(investor, project,ctrl.selected_fm)
          })
          return amounts.reduce(function(a,b){return a+b}, 0)
        }
        //MUST CHANGE THESE FUNCTIONS
        //functions for go to buttons
        ctrl.goToIsValid = function(){
          return (ctrl.select_fm >= 1 && ctrl.select_fm <= ctrl.fm.length && Number.isInteger(ctrl.select_fm)) ? true : false
        }
        ctrl.goTo = function(){
          //recalculate journal table
          ctrl.selected_fm = ctrl.select_fm;
          ctrl.SearchFMInfo()
          ctrl.changeFMInfo()
        }
        ctrl.goToLast = function(){
          if(ctrl.selected_fm == ctrl.fm[ctrl.fm.length - 1].id){ return }
          ctrl.selected_fm = ctrl.fm[ctrl.fm.length - 1].id
          ctrl.SearchFMInfo()
          ctrl.changeFMInfo()
        }
        ctrl.goToNext = function(){
          if(ctrl.selected_fm == ctrl.fm[ctrl.fm.length - 1].id){ return }
          var index = ctrl.fm.map(fm => fm.id).indexOf(ctrl.selected_fm)
          ctrl.selected_fm = ctrl.fm[index + 1].id
          ctrl.SearchFMInfo()
          ctrl.changeFMInfo()
        }
        ctrl.goToPrevious = function(){
          if(ctrl.selected_fm == ctrl.fm[0].id){ return }
          var index = ctrl.fm.map(fm => fm.id).indexOf(ctrl.selected_fm)
          ctrl.selected_fm = ctrl.fm[index - 1].id
          ctrl.SearchFMInfo()
          ctrl.changeFMInfo()
        }
    }
}
export default angular.module('JournalCtrl', [
    angularMeteor
])
    .component('journal', {
        templateUrl: 'imports/components/journal/journal.html',
        controller: JournalCtrl
    })
    .config(config);

    function config($stateProvider){
        'ngInject';
        $stateProvider.state('exerciseView.journal',{
            url: '/diario',
            template: '<journal></journal>',
            title:'Diario',
            cache: false
        });
    }
