import angular       from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter      from 'angular-ui-router';
import template      from './projects.html';
import { Amounts }   from '../../api/amounts.js';
import { Balances }  from '../../api/balances.js';
import { FM }        from '../../api/fm.js';
import { Investors } from '../../api/investors.js';
import { Movements } from '../../api/movements.js';
import { Projects }  from '../../api/projects.js';

class ProjectsCtrl {
    constructor($scope, $reactive, $stateParams) {
        'ngInject';
        $reactive(this).attach($scope);
        console.log('-----------------------------------------------------------')
        let ctrl = this
        let exercise_id = Number($stateParams.exercise_id);
        //Toast
        ctrl.showToast = function() {
          var x = document.getElementById('snackbar')
          x.className = x.className + " show";
          setTimeout(function(){ x.className = x.className.replace(" show", ""); }, 1000);
        }
        //Modal
        var modal = document.getElementById('myModal');
        ctrl.openModal = function(method){
          modal.style.display = "block";
        }
        ctrl.closeModal = function(){
          modal.style.display = "none";
        }
        //open close Add project tab
        ctrl.openAddProjectTab = function(){
            ctrl.showAdd = true;
        }
        ctrl.closeAddProjectTab = function(){
            ctrl.showAdd = false;
        }

        //open close Add project tab
        ctrl.openAddProjectTab = function(){
            ctrl.showAdd = true;
        }
        ctrl.closeAddProjectTab = function(){
            ctrl.showAdd = false;
        }
        //call DB for everything
        ctrl.subscribe('movements',function(){
          ctrl.movements = Movements.find().fetch();
        });
        ctrl.subscribe('investors', function(){
          ctrl.investors = Investors.find({'id_exercise':exercise_id}).fetch();
        });
        ctrl.subscribe('projects', function(){
          ctrl.projects = Projects.find({'id_exercise':exercise_id}).fetch();
        });
        ctrl.changeFMInfo = function(){
          if(ctrl.selected_fm && ctrl.fm.length){
            ctrl.table_project = ctrl.fm.find(function(fm){ return fm.id == ctrl.selected_fm }).id_project.toString()
            ctrl.table_movement = ctrl.fm.find(function(fm){ return fm.id == ctrl.selected_fm }).id_movement.toString()
            ctrl.table_date = ctrl.fm.find(function(fm){ return fm.id == ctrl.selected_fm }).Date
          }
        }
        //call DB for fm
        ctrl.subscribe('fm', function(){
          ctrl.fm = FM.find({'id_exercise':exercise_id}).fetch();
          ctrl.subscribe('amounts', function(){return [exercise_id]}, function(){
            ctrl.amounts = Amounts.find({'id_exercise':exercise_id}).fetch();
          });
          ctrl.subscribe('balances', function(){return [exercise_id]}, function(){
            ctrl.balances = Balances.find({'id_exercise':exercise_id}).fetch();
          });
          ctrl.investors.map(function(investor){
            investor.new_amount = ""
          })
          ctrl.selected_fm = ctrl.fm.length;
          ctrl.select_fm = ctrl.fm.length;
          ctrl.changeFMInfo()
        });
        //open close Add project tab
        ctrl.openAddProjectTab = function(){
            ctrl.showAdd = true;
        }
        ctrl.closeAddProjectTab = function(){
            ctrl.showAdd = false;
        }
        //add project function
        ctrl.addProject = function(){
          if(ctrl.new_project && ctrl.new_project_Desc){
            ctrl.call('projects.insert', { Name: ctrl.new_project, description: ctrl.new_project_Desc, id_exercise: exercise_id }, (error, result)=>{
              if(error){
                console.log(error);
                ctrl.message = "No se pudo crear el proyecto"
                ctrl.showToast()
              }else{
                ctrl.message = "¡Se ha creado el proyecto con éxito!"
                ctrl.showToast();
                ctrl.new_project = ""
                ctrl.new_project_Desc = ""
              }
            });
          }
          ctrl.closeModal()
        }
        //set data for projects table
        ctrl.setProjectBalance = function(project){
            if(ctrl.balances){
                var balance = ctrl.balances.filter(function(x){return x.id_project == project.id && x.id_fm == ctrl.fm.length})
                balance = balance.reduce(function(a,b){return a + (b.amount ? b.amount : 0); },0);
                return balance;
            }
        }
        //set data for movements per project
        ctrl.showProjectMovements = function(project){
          project.history = !project.history;
        }
        ctrl.amountsByProject = function(project){
          var amounts = []
          var fm_by_project = ctrl.fm.filter(function(x){return x.id_project==project.id})
          if(fm_by_project.length){
            var movements_by_fm = fm_by_project.map(function(fm){
              return ctrl.amounts.filter(function(y){return y.id_fm==fm.id})
            })
            movements_by_fm.map(function(x){
              return x.map(function(y){amounts.push(y)})
            })
            return amounts
          }
        }
        ctrl.setMovementFrom = function(movement){
          var fm = ctrl.fm.find(function(fm){ return fm.id == movement.id_fm });
          if(fm){
            switch (fm.id_movement) {
              case 1:
                return ctrl.investors.find(function(investor){ return investor.id == movement.id_investor }).Name;
              case 2:
              case 5:
                return "Capital"
              case 3:
                return "Utilidad"
              case 4:
                return ctrl.projects.find(function(project){ return project.id == fm.id_project }).Name;
              default:
                return "error"
            }
          }
        }
        ctrl.setMovementTo = function(movement){
          var fm = ctrl.fm.find(function(fm){ return fm.id == movement.id_fm });
          if(fm){
            switch (fm.id_movement) {
              case 1:
              case 2:
              case 3:
                return ctrl.projects.find(function(project){ return project.id == fm.id_project }).Name;
              case 4:
                return "Capital";
              case 5:
                return ctrl.investors.find(function(investor){ return investor.id == movement.id_investor }).Name;
              default:
              return "error"
            }
          }
        }
        ctrl.setMovementConcept = function(movement){
          var fm = ctrl.fm.find(function(fm){ return fm.id == movement.id_fm });
          if(ctrl.movements.length && fm){
            if(fm.id_movement == 1 || fm.id_movement ==5){
              return ctrl.movements.find(function(mov){ return mov.id == fm.id_movement }).Name
            }else{
              return (ctrl.investors.find(function(investor){ return investor.id == movement.id_investor }).Name +": " + ctrl.movements.find(function(mov){ return mov.id == fm.id_movement }).Name)
            }
          }
        }
        ctrl.getMovementDate = function(movement){
          if(ctrl.fm.length){
            return ctrl.fm.find(function(fm){ return fm.id == movement.id_fm }).Date
          }
        }
        ctrl.getMovementCreatedAt = function(movement){
          if(ctrl.fm.length){
            return ctrl.fm.find(function(fm){ return fm.id == movement.id_fm }).createdAt
          }
        }
        ctrl.getMovementType = function(movement){
          if(ctrl.fm.length){
            return ctrl.fm.find(function(fm){ return fm.id == movement.id_fm }).id_movement
          }
        }
    }
}
export default angular.module('ProjectsCtrl', [
    angularMeteor
])
    .component('projects', {
        templateUrl: 'imports/components/projects/projects.html',
        controller: ProjectsCtrl
    })
    .config(config);

    function config($stateProvider){
        'ngInject';
        $stateProvider.state('exerciseView.projects',{
            url: '/proyectos',
            template: '<projects></projects>',
            title:'Proyectos',
            cache: false
        });
    }
