import angular       from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter      from 'angular-ui-router';
import template      from './exerciseView.html';
import { Amounts }   from '../../api/amounts.js';
import { Balances }  from '../../api/balances.js';
import { FM }        from '../../api/fm.js';
import { Investors } from '../../api/investors.js';
import { Movements } from '../../api/movements.js';
import { Projects }  from '../../api/projects.js';

class ExerciseViewCtrl {
    constructor() {
        'ngInject';
    }
}
export default angular.module('ExerciseViewCtrl', [
    angularMeteor
])
    .component('exerciseView', {
        templateUrl: 'imports/components/exerciseView/exerciseView.html',
        controller: ExerciseViewCtrl
    })
    .config(config);

    function config($stateProvider){
        'ngInject';
        $stateProvider.state('exerciseView',{
            url: '/ejercicio/:exercise_id',
            template: '<exercise-view></exercise-view>'
        });
    }
