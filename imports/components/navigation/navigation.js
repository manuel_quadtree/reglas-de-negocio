import angular       from 'angular';
import angularMeteor from 'angular-meteor';
import template      from './navigation.html';

class NavigationCtrl {
    constructor($scope, $reactive, $stateParams, $location) {
        'ngInject';
        $reactive(this).attach($scope);
        let ctrl = this;
        var url = $location.path()
        var separator = url.lastIndexOf('/')
        var new_url = url.substring(separator, url.length)
        ctrl.getInitialState = function(){
            switch(new_url){
                case '/movimientos': return "movements";
                case '/inversionistas': return "investors";
                case '/proyectos': return "projects";
                case '/diario': return "journal";
            }
        }
        ctrl.current = ctrl.getInitialState()
        //CREATE A NEW DATE OBJECT EACH TIME THE PAGE NAVIGATES TO MOVEMENTS
    }
}
export default angular.module('NavigationCtrl', [
    angularMeteor
])
    .component('navigation', {
        templateUrl: 'imports/components/navigation/navigation.html',
        controller: NavigationCtrl
    })
