import angular       from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter      from 'angular-ui-router';
import template      from './investors.html';
import { Amounts }   from '../../api/amounts.js';
import { Balances }  from '../../api/balances.js';
import { FM }        from '../../api/fm.js';
import { Investors } from '../../api/investors.js';
import { Movements } from '../../api/movements.js';
import { Projects }  from '../../api/projects.js';

class InvestorsCtrl {
    constructor($scope, $reactive, $stateParams) {
        'ngInject';
        $reactive(this).attach($scope);
        let ctrl = this;
        console.log("-----------------------------------------------------------")
        let exercise_id = Number($stateParams.exercise_id);
        //Toast
        ctrl.showToast = function() {
          var x = document.getElementById('snackbar')
          x.className = x.className + " show";
          setTimeout(function(){ x.className = x.className.replace(" show", ""); }, 1000);
        }
        //Modal
        var modal = document.getElementById('myModal');
        ctrl.openModal = function(method){
          modal.style.display = "block";
        }
        ctrl.closeModal = function(){
          modal.style.display = "none";
        }
        //open close Add investor tab functions
        ctrl.openAddInvestorTab = function(){
            ctrl.showAdd = true;
        }
        ctrl.closeAddInvestorTab = function(){
            ctrl.showAdd = false;
        }
        //call DB for everything
        ctrl.subscribe('movements',function(){
          ctrl.movements = Movements.find().fetch();
          console.log("retrieved movements")
        });
        ctrl.subscribe('investors', function(){
          ctrl.investors = Investors.find({'id_exercise':exercise_id}).fetch();
          console.log("retrieved investors")
        });
        ctrl.subscribe('projects', function(){
          ctrl.projects = Projects.find({'id_exercise':exercise_id}).fetch();
          console.log("retrieved projects")
        });
        //call DB for fm
        ctrl.subscribe('fm', function(){
          ctrl.fm = FM.find({'id_exercise':exercise_id}).fetch();
          console.log("retrieved fms")
          ctrl.subscribe('amounts', function(){return [exercise_id]}, function(){
            ctrl.amounts = Amounts.find({'id_exercise':exercise_id}).fetch();
            console.log("retrieved amounts")
          });
          ctrl.subscribe('balances', function(){return [exercise_id]}, function(){
            ctrl.balances = Balances.find({'id_exercise':exercise_id}).fetch();
            console.log("retrieved balances")
          });
          ctrl.investors.map(function(investor){
            investor.new_amount = ""
          })
        });
        //Add Investor function
        ctrl.addInvestor = function(){
          if(ctrl.new_investor && ctrl.new_investor_Desc){
            ctrl.call('investors.insert', { Name: ctrl.new_investor, description: ctrl.new_investor_Desc, active: true, id_exercise: exercise_id }, (error, result)=>{
              if(error){
                console.log(error);
                ctrl.message = "No se pudo crear el inversor"
                ctrl.showToast()
              }else{
                ctrl.message = "¡Se ha creado el inversor con éxito!"
                ctrl.showToast();
                ctrl.new_investor = ""
                ctrl.new_investor_Desc = ""
              }
            });
          }
          ctrl.closeModal()
        }
        //set data for investors table
        ctrl.setInvestorBalance = function(investor){
            if(ctrl.balances){
                var balance = ctrl.balances.find(function(x){ return investor.id == x.id_investor && x.id_fm == ctrl.fm.length && x.id_project == 0 })
                return balance ? balance.amount : 0;
            }
        }
        ctrl.setInvestorSum = function(investor){
            if(ctrl.balances){
                var sum = ctrl.balances.filter(function(x){return x.id_investor == investor.id && x.id_fm == ctrl.fm.length})
                sum = sum.reduce(function(a, b){return (a*100 + (b.amount ? b.amount : 0)*100)/100; }, 0);
                return sum
            }
        }
        ctrl.setInvestorAmount = function(investor){
          if(ctrl.selected_fm){
            var amount = ctrl.amounts.find(function(x){ return x.id_investor == investor.id && x.id_fm == ctrl.selected_fm });
            return amount ? amount.amount : 0;
          }
        }
        ctrl.setInvestorStatus = function (investor){
          return investor.active ? "Habilitado" : "Inhabilitado"
        }
        ctrl.showInvestorMovements = function(investor){
          investor.history = !investor.history;
        }
        ctrl.toggleInvestor = function(investor){
          ctrl.call('investors.toggle', investor, (error, result)=>{
            if(error){
              console.log(error);
              ctrl.message = "No se pudo actualizar estado";
              ctrl.showToast();
            }else{
              ctrl.message = "Estado actualizado";
              ctrl.showToast();
            }
          })
        }
        //set data for movements per investor
        ctrl.amountsByInvestor = function(investor){
          //console.log(ctrl.amounts.filter(function(x){return x.id_investor==investor.id}))
          return ctrl.amounts.filter(function(x){return x.id_investor==investor.id})
        }
        ctrl.setMovementFrom = function(movement){
          var fm = ctrl.fm.find(function(fm){ return fm.id == movement.id_fm });
          if(fm){
            switch (fm.id_movement) {
              case 1:
                return ctrl.investors.find(function(investor){ return investor.id == movement.id_investor }).Name;
              case 2:
              case 5:
                return "Capital"
              case 3:
                return "Utilidad"
              case 4:
                return ctrl.projects.find(function(project){ return project.id == fm.id_project }).Name;
              default:
                return "error"
            }
          }
        }
        ctrl.setMovementTo = function(movement){
          var fm = ctrl.fm.find(function(fm){ return fm.id == movement.id_fm });
          if(fm){
            switch (fm.id_movement) {
              case 1:
              case 2:
              case 3:
                return ctrl.projects.find(function(project){ return project.id == fm.id_project }).Name;
              case 4:
                return "Capital";
              case 5:
                return ctrl.investors.find(function(investor){ return investor.id == movement.id_investor }).Name;
              default:
              return "error"
            }
          }
        }
        ctrl.setMovementConcept = function(movement){
          var fm = ctrl.fm.find(function(fm){ return fm.id == movement.id_fm });
          if(ctrl.movements.length && fm){
            if(fm.id_movement == 1 || fm.id_movement ==5){
              return ctrl.movements.find(function(mov){ return mov.id == fm.id_movement }).Name
            }else{
              return (ctrl.investors.find(function(investor){ return investor.id == movement.id_investor }).Name +": " + ctrl.movements.find(function(mov){ return mov.id == fm.id_movement }).Name)
            }
          }
        }
        ctrl.getMovementDate = function(movement){
          if(ctrl.fm.length){
            return ctrl.fm.find(function(fm){ return fm.id == movement.id_fm }).Date
          }
        }
        ctrl.getMovementCreatedAt = function(movement){
          if(ctrl.fm.length){
            return ctrl.fm.find(function(fm){ return fm.id == movement.id_fm }).createdAt
          }
        }
        ctrl.getMovementType = function(movement){
          if(ctrl.fm.length){
            return ctrl.fm.find(function(fm){ return fm.id == movement.id_fm }).id_movement
          }
        }
    }
}
export default angular.module('InvestorsCtrl', [
    angularMeteor
])
    .component('investors', {
        templateUrl: 'imports/components/investors/investors.html',
        controller: InvestorsCtrl
    })
   .config(config);

    function config($stateProvider){
        'ngInject';
        $stateProvider.state('exerciseView.investors',{
            url: '/inversionistas',
            template: '<investors></investors>',
            cache: false
        });
    }
