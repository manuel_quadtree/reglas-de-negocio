import angular       from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter      from 'angular-ui-router';
import template      from './exercises.html';
import { Exercises } from '../../api/exercises.js';

class ExercisesCtrl {
    constructor($scope, $reactive, $stateParams) {
        'ngInject';
        $reactive(this).attach($scope);
        let ctrl = this;
        //Modal
        var modal = document.getElementById('myModal');
        ctrl.openModal = function(){
          modal.style.display = "block";
        }
        ctrl.closeModal = function(){
          modal.style.display = "none";
        }
        //Toast
        ctrl.showToast = function() {
          var x = document.getElementById('snackbar')
          x.className = x.className + " show";
          setTimeout(function(){ x.className = x.className.replace(" show", ""); }, 1000);
        }
        //open close Add exercises tab
        ctrl.openAddExerciseTab = function(){
            ctrl.showAdd = true;
        }
        ctrl.closeAddExerciseTab = function(){
            ctrl.showAdd = false;
        }
        ctrl.subscribe('exercises',function(){
          ctrl.exercises = Exercises.find().fetch();
        });
        //add exercise function
        ctrl.addExercise = function(){
          if(ctrl.new_exercise){
            var new_exercise = {
                id : ctrl.exercises.length + 1,
                name: ctrl.new_exercise
            }
            ctrl.call('exercises.insert', new_exercise, (error, result)=>{
              if(error){
                console.log(error)
                ctrl.message = "No se pudo crear el ejercicio"
                ctrl.showToast()
              }else{
                ctrl.message = "El ejercicio se ha creado con éxito"
                ctrl.showToast()
              }
            });
            ctrl.new_exercise = ""
            ctrl.closeModal();
            }else{
              ctrl.message = "Escriba un nombre para el ejercicio"
              ctrl.showToast()
            }
        }
    }
}

export default angular.module('ExercisesCtrl', [
    angularMeteor
])
    .component('exercises', {
        templateUrl: 'imports/components/exercises/exercises.html',
        controller: ExercisesCtrl
    })
    .config(config);

    function config($stateProvider){
        'ngInject';
        $stateProvider.state('exercises',{
            url: '/ejercicios',
            template: '<exercises></exercises>'
        });
    }
