import angular       from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter      from 'angular-ui-router';
import template      from './movements.html';
import { Amounts }   from '../../api/amounts.js';
import { Balances }  from '../../api/balances.js';
import { FM }        from '../../api/fm.js';
import { Investors } from '../../api/investors.js';
import { Movements } from '../../api/movements.js';
import { Projects }  from '../../api/projects.js';

class MovementsCtrl {
    constructor($scope, $reactive, $stateParams) {
        'ngInject';
        $reactive(this).attach($scope);
        let ctrl = this;
        console.log('-----------------------------------------------------------')
        let exercise_id = Number($stateParams.exercise_id);
        //Toast
        ctrl.showToast = function() {
          var x = document.getElementById('snackbar')
          x.className = x.className + " show";
          setTimeout(function(){ x.className = x.className.replace(" show", ""); }, 2000);
        }
        //Modal
        var modal = document.getElementById('myModal');
        ctrl.openModal = function(method){
          modal.style.display = "block";
          ctrl.method = method;
        }
        ctrl.closeModal = function(){
          modal.style.display = "none";
        }
        //open close Add movements tab
        ctrl.openAddMovementTab = function(){
            ctrl.showAdd = true;
        }
        ctrl.closeAddMovementTab = function(){
            ctrl.showAdd = false;
        }
        //call DB for everything
        ctrl.subscribe('movements',function(){
          ctrl.movements = Movements.find().fetch();
          console.log(ctrl.movements)
          console.log("received movements")
        });
        ctrl.subscribe('investors', function(){
          ctrl.investors = Investors.find({'id_exercise':exercise_id}).fetch();
          console.log("received investors")
        });
        ctrl.subscribe('projects', function(){
          ctrl.projects = Projects.find({'id_exercise':exercise_id}).fetch();
          console.log("received projects")
        });
        //call DB for fm
        ctrl.subscribe('fm', function(){
          ctrl.fm = FM.find({'id_exercise':exercise_id}).fetch();
          console.log("received fms")
          //console.log("Retrieved fms")
          ctrl.investors.map(function(investor){
            investor.new_amount = ""
          })
          ctrl.selected_fm = ctrl.fm.length;
          ctrl.select_fm = ctrl.fm.length;

          ctrl.subscribe('amounts', function(){return [exercise_id]}, function(){
            ctrl.amounts = Amounts.find({'id_exercise':exercise_id}).fetch();
            console.log("received amounts")
            //console.log("Retrieved amounts")
          });
          ctrl.subscribe('balances', function(){return [exercise_id]}, function(){
            ctrl.balances = Balances.find({'id_exercise':exercise_id}).fetch();
            console.log("received balances")
            //console.log("Retrived balances")
          });
          ctrl.dateType = "auto";
          ctrl.changeDateType = function(){
            if(ctrl.dateType == 'auto'){
              ctrl.date = ""
            }else if(ctrl.dateType == 'manual'){
              ctrl.date = new Date()
            }else{
              console.log('error seleccionando tipo de fecha; se utilizará automática')
            }
          }
        });
        ctrl.initializeData = function(){
          ctrl.new_observation = "";
          ctrl.selectedProject = "";
          ctrl.revenue = "";
          ctrl.transferInvestor = "";
          ctrl.transferFrom = "";
          ctrl.transferTo = "";
          ctrl.transfer_amount = "";
          ctrl.autoInvest_amount = "";
          ctrl.investors.map(function(investor){
            investor.new_amount="";
          })
        }
        //FAST VALIDATION
        ctrl.validateMovement = function(){
          //validate that the data registered in each movement is valid
          switch(ctrl.selectedData){
            case "1":
            case "5":
              var amount = ctrl.investors.filter(function(x){return x.new_amount});
              return amount.length ? true : false;
            case "2":
              if (ctrl.autoInvest){
                return (ctrl.autoInvest_amount && ctrl.selectedProject  && (ctrl.autoInvest_amount<=ctrl.setProjectBalance(ctrl.projects[0]))) ? true : false;
              }else{
                var amount = ctrl.investors.filter(function(x){return x.new_amount});
                return (amount.length && ctrl.selectedProject) ? true : false;
              }
            case "4":
              var amount = ctrl.investors.filter(function(x){return x.new_amount});
              return (amount.length && ctrl.selectedProject) ? true : false;
            case "3":
              return (ctrl.revenue && ctrl.selectedProject) ? true : false;
            case "6":
              var validater = ctrl.balances.find(function(x){return x.id_fm==ctrl.fm.length && x.id_project==Number(ctrl.transferFrom) && x.id_investor == Number(ctrl.transferInvestor)})
              validater = validater ? validater.amount : 0
              return ((validater>=ctrl.transfer_amount)&& ctrl.transfer_amount && ctrl.transferInvestor && ctrl.transferFrom && ctrl.transferTo && (ctrl.transferFrom!=ctrl.transferTo)) ? true : false
            default:
              return false
          }
        }
        ctrl.validateReturn = function(){
          switch (ctrl.selectedData) {
            case "4":
              if (!ctrl.selectedProject){
                return false
              }else{
                if(ctrl.setProjectBalance(ctrl.projects.find(function(x){return x.id == Number(ctrl.selectedProject)}))){
                  return true
                }else{
                  return false
                }
              }
              break;
            case "5":
              if(ctrl.setProjectBalance(ctrl.projects.find(function(x){return x.id == 0}))){
                return true
              }else {
                return false
              }
              break;
            default:
            return false
          }
        }
        //AUTOINVEST
        ctrl.autoInvest = true;
        ctrl.autoInvestFM = function(){
          if(ctrl.validateMovement()){
            var investors_with_balances = ctrl.investors.map(function(investor){
              investor.balance = ctrl.setInvestorBalance(investor)
              return investor
            })
            ctrl.new_observation = ctrl.new_observation ? ("Auto: "+ctrl.new_observation) : "Auto"
            if(ctrl.autoInvest_amount>30000){
              var small_investors = investors_with_balances.filter(function(investor){return investor.balance <= 30000 && investor.active})
              if(small_investors.length){
                var to_assign = ctrl.autoInvest_amount;
                small_investors.sort(function(a,b){return a.balance - b.balance})
                small_investors.map(function(investor){
                  investor.new_amount = (to_assign>=investor.balance ? investor.balance : to_assign);
                  to_assign = (to_assign*100-investor.new_amount*100)/100;
                })
                if(to_assign){
                  var big_investors = investors_with_balances.filter(function(investor){return investor.balance > 30000 && investor.active})
                  var sum_of_big = 0
                  big_investors.map(function(investor){
                    sum_of_big = (sum_of_big*100+investor.balance*100)/100
                  })
                  var cents=0;
                  big_investors.map(function(investor){
                    var ratio = investor.balance/sum_of_big;
                    var amount = to_assign*ratio;
                    var remainder = (amount*100) % 1;
                    amount = (Math.floor(amount*100))/100;
                    cents += remainder;
                    investor.new_amount = amount;
                  })
                  cents = Math.round(cents);
                  for (let i = 0; i<cents; i++){
                    var rand_Inv =  big_investors[Math.floor(Math.random()*big_investors.length)];
                    rand_Inv.new_amount = (Math.floor((rand_Inv.new_amount*100)+1))/100;
                  }
                }
              }else{
                var cents=0;
                var active_investors = investors_with_balances.filter(function(investor){return investor.active});
                var sum_of_imvestors = 0;
                active_investors.map(function(investor){
                  sum_of_imvestors = (sum_of_imvestors*100+investor.balance*100)/100;
                })
                active_investors.map(function(investor){
                  var ratio = investor.balance/sum_of_imvestors;
                  var amount = ctrl.autoInvest_amount*ratio;
                  var remainder = (amount*100) % 1;
                  amount = (Math.floor(amount*100))/100;
                  cents += remainder;
                  investor.new_amount = amount;
                })
                cents = Math.round(cents);
                for (let i = 0; i<cents; i++){
                  var rand_Inv =  ctrl.investors[Math.floor(Math.random()*ctrl.investors.length)];
                  rand_Inv.new_amount = (Math.floor((rand_Inv.new_amount*100)+1))/100;
                }
              }
            }else{
              var max = Math.max.apply(null,investors_with_balances.map(function(investor){return investor.balance}));
              var big_investors = investors_with_balances.filter(function(investor){return investor.balance == max && investor.active});
              var amount = ctrl.autoInvest_amount/big_investors.length;
              var cents = Math.round(((amount*100) % 1)*big_investors.length);
              amount = (Math.floor(amount*100))/100;
              if(big_investors[0].balance>=amount+cents){
                big_investors.map(function(investor){
                  investor.new_amount=amount;
                })
                for (let i = 0; i<cents; i++){
                  var rand_Inv =  big_investors[Math.floor(Math.random()*big_investors.length)];
                  rand_Inv.new_amount = (Math.floor((rand_Inv.new_amount*100)+1))/100;
                }
              }else{
                var to_assign = ctrl.autoInvest_amount;
                big_investors.map(function(investor){
                  investor.new_amount = investor.balance;
                  to_assign = (to_assign*100-investor.balance*100)/100;
                })
                small_investors = ctrl.investors.filter(function(investor){return investor.balance != max && investor.active});
                var cents = 0;
                var sum_of_small = 0;
                small_investors.map(function(investor){
                  sum_of_small = (sum_of_small*100+investor.balance*100)/100;
                })
                small_investors.map(function(investor){
                  var ratio = investor.balance/(sum_of_small)
                  var amount = to_assign*ratio;
                  var remainder = (amount*100) % 1;
                  amount = (Math.floor(amount*100))/100;
                  cents += remainder;
                  investor.new_amount = amount;
                })
                cents = Math.round(cents);
                for (let i = 0; i<cents; i++){
                  var rand_Inv =  small_investors[Math.floor(Math.random()*small_investors.length)];
                  rand_Inv.new_amount = (Math.floor((rand_Inv.new_amount*100)+1))/100;
                }
              }
            }
            //CALL TO REGISTER FM
            ctrl.registerFM()
          }
        }
        //REGISTER FM
        ctrl.registerFM = function(){
          //adds to DB depending on selected movement
          if(ctrl.validateMovement()){
            //console.log("entered to registerFM")
            if(ctrl.fm.length){
              var new_balances = ctrl.projects.map(function(project){
                var balances_per_project = ctrl.investors.map(function(investor){
                  return {id_exercise: exercise_id, id_investor: investor.id, id_project: project.id, id_fm: ctrl.fm.length + 1, amount: ctrl.setInvestorByProjectAmount(investor, project, ctrl.fm.length)}
                })
                return balances_per_project
              })
            }else{
              var new_balances = ctrl.projects.map(function(project){
                var balances_per_project = ctrl.investors.map(function(investor){
                  return {id_exercise: exercise_id, id_investor: investor.id, id_project: project.id, id_fm: 1, amount: 0}
                })
                return balances_per_project
              })
            }
            var positiveBalances = true;
            new_balances = [].concat.apply([], new_balances);
            var balances_arr = [];
            var amounts_arr = [];
            var new_fm
            var id_amounts=ctrl.amounts.length+1;
            //validation
            var setDB = true
            switch(ctrl.selectedData){
              case "1":
                ctrl.investors.map(function(investor){
                  new_balances.map(function(balance){
                    if(balance.id_investor == investor.id && balance.id_project == 0){
                      balance.amount = investor.new_amount ? (balance.amount*100 + investor.new_amount*100)/100 : balance.amount
                    }
                  })
                })
                break;
              case "2":
                ctrl.investors.map(function(investor){
                  new_balances.map(function(balance){
                    if(balance.id_investor == investor.id && balance.id_project == 0){
                      balance.amount = investor.new_amount ? (balance.amount*100 - investor.new_amount*100)/100 : balance.amount
                    }else if( balance.id_investor == investor.id && balance.id_project == Number(ctrl.selectedProject)){
                      balance.amount = investor.new_amount ? (balance.amount*100 + investor.new_amount*100)/100 : balance.amount
                    }
                  })
                })
                break;
              case "3":
                var cents=0;
                ctrl.investors.map(function(investor){
                  new_balances.map(function(balance){
                    var project_balance = ctrl.projects.find(function(x){return x.id == Number(ctrl.selectedProject)})
                    if(balance.id_investor == investor.id && balance.id_project == Number(ctrl.selectedProject)){
                      var amount = balance.amount*(ctrl.revenue)/ctrl.setProjectBalance(project_balance);
                      var remainder = ((amount*100) % 1)/100;
                      cents += remainder;
                      amount = (Math.floor(amount*100))/100;
                      balance.amount = ((balance.amount*100)+(amount*100))/100;
                      if(balance.amount){
                        amounts_arr.push({id_exercise: exercise_id, id:id_amounts, amount: amount, id_investor: investor.id, id_fm: ctrl.fm.length + 1 });
                        id_amounts+=1;
                      }
                    }
                  })

                })
                cents= Math.round(cents*100);
                for (let i = 0; i<cents; i++){
                  var rand_Inv =  amounts_arr[Math.floor(Math.random()*amounts_arr.length)];
                  rand_Inv.amount = (Math.floor((rand_Inv.amount*100)+1))/100;
                  var balance_rand = new_balances.find(function(x){return x.id_investor == rand_Inv.id_investor && x.id_project == Number(ctrl.selectedProject)});
                  balance_rand.amount = (Math.floor((balance_rand.amount*100)+1))/100;
                }
                break;
              case "4":
                ctrl.investors.map(function(investor){
                  new_balances.map(function(balance){
                    if(balance.id_investor == investor.id && balance.id_project == 0){
                      balance.amount = investor.new_amount ? (balance.amount*100 + investor.new_amount*100)/100 : balance.amount
                    }else if( balance.id_investor == investor.id && balance.id_project == Number(ctrl.selectedProject)){
                      balance.amount = investor.new_amount ? (balance.amount*100 - investor.new_amount*100)/100 : balance.amount
                    }
                  })
                })
                break;
              case "5":
                ctrl.investors.map(function(investor){
                  new_balances.map(function(balance){
                    if(balance.id_investor == investor.id && balance.id_project == 0){
                      balance.amount = investor.new_amount ? (balance.amount*100 - investor.new_amount*100)/100 : balance.amount
                    }
                  })
                })
                break;
              default:
               //console.log('Please stop messing with my beautiful work')
               var setDB = false
            }
            if(setDB){
              //console.log(new_balances)
              new_balances.map(function(balance){
                if(balance.amount){
                  if(balance.amount>=0){
                    balances_arr.push(balance)
                  }else{
                    positiveBalances = false;
                  }
                }
              })
              if(ctrl.selectedData == "3"){
                validation_arr = balances_arr.filter(function(x){return x.id_project == Number(ctrl.selectedProject)})
                if (!validation_arr.length){
                  positiveBalances = false;
                }
              }
              //generate amounts array
              if(ctrl.selectedData != "3"){
                ctrl.investors.map(function(investor){
                  if(investor.new_amount){
                    amounts_arr.push({id_exercise: exercise_id, id:id_amounts, amount: investor.new_amount, id_investor: investor.id, id_fm: ctrl.fm.length + 1 });
                    id_amounts+=1;
                  }
                })
              }
              if(positiveBalances){
                new_fm = {
                  id: ctrl.fm.length + 1,
                  createdAt: new Date(),
                  Date: ctrl.date && ctrl.dateType == 'manual' ? new Date(ctrl.date) : new Date(),
                  id_exercise: exercise_id,
                  id_movement: Number(ctrl.selectedData),
                  id_project: ctrl.getSelectedProject(),
                  observation: ctrl.new_observation ? ctrl.new_observation : "",
                  reverted: false
                }
                //console.log(new_fm,balances_arr,amounts_arr)
                //console.log("started connection to DB")
                ctrl.call('fm.insert', new_fm, amounts_arr, balances_arr, (error, result)=>{
                  //console.log(ctrl.fm)
                  if(error){
                    //console.log(error)
                    ctrl.message = "No se pudo crear su movimiento correctamente"
                    ctrl.showToast()
                  }else{
                    ctrl.message = "¡Su movimiento se ha creado con éxito!"
                    ctrl.showToast()
                  }
                })
              }else{
                //console.log("No hay varo para tu movimiento :'(")
                ctrl.message = "El movimiento tiene cantidades incorrectas; favor de revisarlo"
                ctrl.showToast()
              }
              ctrl.initializeData();
            }
          }
        }
        //REGISTER TRANSFER (COULD THIS BE DONE IN REGISTER FM?)
        ctrl.registerTransfer = function(){
          if(ctrl.validateMovement()){
            var amount = ctrl.transfer_amount;
            var observation = ctrl.new_observation ? "Traspaso: "+ctrl.new_observation : "Traspaso"
            var new_balances_1
            if(ctrl.fm.length){
              new_balances_1 = ctrl.projects.map(function(project){
                var balances_per_project = ctrl.investors.map(function(investor){
                  return {id_exercise: exercise_id, id_investor: investor.id, id_project: project.id, id_fm: ctrl.fm.length + 1, amount: ctrl.setInvestorByProjectAmount(investor, project, ctrl.fm.length)}
                })
                return balances_per_project
              })
            }else{
              new_balances_1 = ctrl.projects.map(function(project){
                var balances_per_project = ctrl.investors.map(function(investor){
                  return {id_exercise: exercise_id, id_investor: investor.id, id_project: project.id, id_fm: 1, amount: 0}
                })
                return balances_per_project
              })
            }
            new_balances_1 = [].concat.apply([], new_balances_1);
            var balances_arr_1 = [];
            var amounts_arr_1 = [];
            var new_fm_1;
            var id_amounts=ctrl.amounts.length+1;
            var positiveBalances = true;
            new_balances_1.map(function(balance){
              if(balance.id_investor == Number(ctrl.transferInvestor) && balance.id_project == 0){
                balance.amount = amount ? (balance.amount*100 + amount*100)/100 : balance.amount
              }else if( balance.id_investor == Number(ctrl.transferInvestor) && balance.id_project == Number(ctrl.transferFrom)){
                balance.amount = amount ? (balance.amount*100 - amount*100)/100 : balance.amount
              }
            })
            new_balances_1.map(function(balance){
              if(balance.amount){
                if(balance.amount>=0){
                  balances_arr_1.push(balance)
                }else{
                  positiveBalances = false;
                }
              }
            })
            amounts_arr_1.push({id_exercise: exercise_id, id:id_amounts, amount: amount, id_investor: Number(ctrl.transferInvestor), id_fm: ctrl.fm.length + 1 });
            id_amounts++
            new_fm_1 = {
              id: ctrl.fm.length + 1,
              createdAt: new Date(),
              Date: ctrl.date && ctrl.dateType == 'manual' ? new Date(ctrl.date) : new Date(),
              id_exercise: exercise_id,
              id_movement: 4,
              id_project: Number(ctrl.transferFrom),
              observation: observation,
              reverted: false
            }
            if(positiveBalances){
              //console.log(new_fm_1,balances_arr_1,amounts_arr_1)
              ctrl.call('fm.insert', new_fm_1, amounts_arr_1, balances_arr_1, (error, result)=>{
                //console.log(ctrl.fm)
                if(error){
                  //console.log(error)
                  ctrl.message = "No se pudo crear su movimiento correctamente"
                  ctrl.showToast()
                }else{
                  ctrl.message = "¡Su movimiento se ha creado con éxito!"
                  ctrl.showToast()
                }
              })
              var new_balances_2 = new_balances_1.map(function(balance){
                return {id_exercise: exercise_id, id_investor: balance.id_investor, id_project: balance.id_project, id_fm: balance.id_fm+1, amount: balance.amount}
              })
              var balances_arr_2 = [];
              var amounts_arr_2 = [];
              var new_fm_2;
              new_balances_2.map(function(balance){
                if(balance.id_investor == Number(ctrl.transferInvestor) && balance.id_project == 0){
                  balance.amount = amount ? (balance.amount*100 - amount*100)/100 : balance.amount
                }else if( balance.id_investor == Number(ctrl.transferInvestor) && balance.id_project == Number(ctrl.transferTo)){
                  balance.amount = amount ? (balance.amount*100 + amount*100)/100 : balance.amount
                }
              })
              new_balances_2.map(function(balance){
                if(balance.amount){
                  balances_arr_2.push(balance)
                }
              })
              amounts_arr_2.push({id_exercise: exercise_id, id:id_amounts, amount: amount, id_investor: Number(ctrl.transferInvestor), id_fm: new_balances_2[0].id_fm });
              new_fm_2 = {
                id: new_balances_2[0].id_fm,
                createdAt: new Date(),
                Date: ctrl.date && ctrl.dateType == 'manual' ? new Date(ctrl.date) : new Date(),
                id_exercise: exercise_id,
                id_movement: 2,
                id_project: Number(ctrl.transferTo),
                observation: observation,
                reverted: false
              }
              //console.log(new_fm_2,balances_arr_2,amounts_arr_2)
              ctrl.call('fm.insert', new_fm_2, amounts_arr_2, balances_arr_2, (error, result)=>{
                //console.log(ctrl.fm)
                if(error){
                  //console.log(error)
                  ctrl.message = "No se pudo crear su movimiento correctamente"
                  ctrl.showToast()
                }else{
                  ctrl.message = "¡Su movimiento se ha creado con éxito!"
                  ctrl.showToast()
                }
              })
            }else{
              //console.log("No hay varo para tu movimiento :'(")
              ctrl.message = "El movimiento tiene cantidades incorrectas; favor de revisarlo"
              ctrl.showToast()
            }

          }
        }
        //REVERT MOVEMENT
        ctrl.revertMovement = function(fm){
          if(!fm.reverted){
            var actualData = ctrl.selectedData;
            //console.log(fm)
            if(!fm.observation.startsWith("Traspaso")){
              var valid = true;
              switch (fm.id_movement){
                case 1:
                  ctrl.selectedData = "5";
                  ctrl.investors.map(function(investor){
                    ctrl.amounts.map(function(amount){
                      if(amount.id_fm == fm.id && amount.id_investor == investor.id){
                        investor.new_amount=amount.amount;
                      }
                      if(investor.new_amount>investor.balance){
                        valid = false;
                      }
                    })
                  })
                  break;
                case 2:
                  ctrl.selectedData = "4";
                  ctrl.selectedProject = fm.id_project.toString();
                  ctrl.investors.map(function(investor){
                    ctrl.amounts.map(function(amount){
                      if(amount.id_fm == fm.id && amount.id_investor == investor.id){
                        investor.new_amount=amount.amount;
                      }
                      var selectedBalance = ctrl.balances.find(function(x){return x.id_fm==ctrl.fm.length && x.id_project==Number(ctrl.selectedProject) && x.id_investor == investor.id})
                      selectedBalance = selectedBalance ? selectedBalance.amount : 0;
                      if(investor.new_amount>selectedBalance){
                        valid = false;
                      }
                    })
                  })
                  break;
                case 4:
                  ctrl.selectedData = "2";
                  ctrl.selectedProject = fm.id_project.toString();
                  ctrl.investors = ctrl.investors.map(function(investor){
                    ctrl.amounts.map(function(amount){
                      if(amount.id_fm == fm.id && amount.id_investor == investor.id){
                        investor.new_amount = amount.amount;
                      }
                      var selectedBalance = ctrl.balances.find(function(x){return x.id_fm==ctrl.fm.length && x.id_project==0 && x.id_investor == investor.id})
                      selectedBalance = selectedBalance ? selectedBalance.amount : 0;
                      if(investor.new_amount>selectedBalance){
                        valid = false;
                      }
                    })
                    return investor
                  })
                  //console.log(ctrl.investors)
                  break;
                case 5:
                  ctrl.selectedData = "1";
                  ctrl.investors.map(function(investor){
                    ctrl.amounts.map(function(amount){
                      if(amount.id_fm == fm.id && amount.id_investor == investor.id){
                        investor.new_amount=amount.amount;
                      }
                    })
                  })
                  break;
                default:
                  //console.log("Stop messing please");
                  valid=false
               }
              if(valid){
                ctrl.new_observation = "Reversión de FM: "+fm.id;
                ctrl.registerFM()
                ctrl.call('fm.revert', fm, (error, result)=>{
                  if(error){
                    //console.log(error)
                    ctrl.message = "No se pudo crear su movimiento"
                    ctrl.showToast()
                  }else{
                    ctrl.message = "Movimiento revertido"
                    ctrl.showToast()
                  }
                })
              }else{
                ctrl.message = "El movimieto no puede ser revertido de momento"
                ctrl.showToast()
                ctrl.selectedData = actualData;
                ctrl.initializeData()
              }
            }else{
              //console.log("traspaso")
              ctrl.message = "Los traspasos deben revertirse de forma manual"
              ctrl.showToast()
            }
            ctrl.initializeData();
          }
        }
        ctrl.choseMethod = function(){
          switch(ctrl.method){
            case 3:
              switch (ctrl.selectedData){
                case "2":
                (ctrl.autoInvest) ? ctrl.autoInvestFM() : ctrl.registerFM()
                  break;
                case "6":
                  ctrl.registerTransfer()
                  break;
                default:
                  ctrl.registerFM()
              }
              break;
            case 4:
              switch(ctrl.selectedData){
                case "4":
                  ctrl.investors.map(function(investor){
                    var investor_balance = ctrl.balances.find(function(x){return x.id_fm==ctrl.fm.length && x.id_project==Number(ctrl.selectedProject) && x.id_investor == investor.id})
                    investor.new_amount = investor_balance ? investor_balance.amount : 0
                  })
                  ctrl.registerFM()
                  break;
                case "5":
                  ctrl.investors.map(function(investor){
                    investor.new_amount = ctrl.setInvestorBalance(investor)
                  })
                  ctrl.registerFM()
                  break;
                default:
                  //console.log("I dare you to mess with the application again, I double dare you motherfucker")
              }
              break;
            default:
              //console.log("Stop messing with the application >:(")
          }
          ctrl.closeModal()
        }
        //set data for movements table
        ctrl.setFmTotal = function(fm){
            if(ctrl.amounts){
                var total = 0;
                var fmMovements = ctrl.amounts.filter(function(x){return x.id_fm == fm.id})
                fmMovements.map(function(x){total += x.amount})
                return total;
            }
        }
        //set data for movement per FM
        ctrl.showFmMovements = function(fm){
          fm.history = !fm.history;
        }
        ctrl.amountsByFm = function(fm){
          return ctrl.amounts.filter(function(x){return x.id_fm == fm.id})
        }
        ctrl.setInvestorBalance = function(investor){
            if(ctrl.balances){
                var balance = ctrl.balances.find(function(x){ return investor.id == x.id_investor && x.id_fm == ctrl.fm.length && x.id_project == 0 })
                return balance ? balance.amount : 0;
            }
        }
        //Functions for autmatic invesment
        ctrl.setProjectBalance = function(project){
            if(ctrl.balances && project){
                var balance = ctrl.balances.filter(function(x){return x.id_project == project.id && x.id_fm == ctrl.fm.length})
                balance = balance.reduce(function(a,b){return a + (b.amount ? b.amount : 0); },0);
                return balance;
            }
        }
        ctrl.setInvestorByProjectAmount = function(investor, project, fm){
          var amount = ctrl.balances.find(function(x){ return x.id_investor == investor.id && x.id_project == project.id && x.id_fm == fm})
          amount = amount ? amount.amount : 0;
          return amount
        }
        ctrl.selectedProjectBalance = function(investor){
          if(!ctrl.selectedProject){
            return "Seleccione un proyecto"
          }else{
            var selectedBalance = ctrl.balances.find(function(x){return x.id_fm==ctrl.fm.length && x.id_project==Number(ctrl.selectedProject) && x.id_investor == investor.id})
            return selectedBalance ? selectedBalance.amount : 0
          }
        }
        //Functions for transfers
        ctrl.transferBalanceFrom = function(){
          if(!ctrl.transferInvestor){
            return "Seleccione un inversor"
          }else if(!ctrl.transferFrom){
            return "Seleccione un proyecto"
          }else{
            var from_balance = ctrl.balances.find(function(x){return x.id_fm==ctrl.fm.length && x.id_project==Number(ctrl.transferFrom) && x.id_investor == Number(ctrl.transferInvestor)})
            return from_balance ? from_balance.amount : 0
          }
        }
        ctrl.transferBalanceTo = function(){
          if(!ctrl.transferInvestor){
            return "Seleccione un inversor"
          }else if(!ctrl.transferTo){
            return "Seleccione un proyecto"
          }else{
            var to_balance = ctrl.balances.find(function(x){return x.id_fm==ctrl.fm.length && x.id_project==Number(ctrl.transferTo) && x.id_investor == Number(ctrl.transferInvestor)})
            return to_balance ? to_balance.amount : 0
          }
        }
        ctrl.getSelectedProject = function(){
          if(ctrl.selectedData == "1" || ctrl.selectedData == "5"){
            return 0
          }else{
            return Number(ctrl.selectedProject)
          }
        }
        //Inputs per investor functions
        ctrl.disableInvestorInput = function(investor){
          if(ctrl.selectedData == 1 || ctrl.selectedDate == 2){
            return investor.active ? true : false
          }else{
            return true
          }
        }
        ctrl.setMovementFrom = function(movement){
          var fm = ctrl.fm.find(function(fm){ return fm.id == movement.id_fm });
          if(fm){
            switch (fm.id_movement) {
              case 1:
                return ctrl.investors.find(function(investor){ return investor.id == movement.id_investor }).Name;
              case 2:
              case 5:
                return "Capital"
              case 3:
                return "Utilidad"
              case 4:
                return ctrl.projects.find(function(project){ return project.id == fm.id_project }).Name;
              default:
                return "error"
            }
          }
        }
        ctrl.setMovementTo = function(movement){
          var fm = ctrl.fm.find(function(fm){ return fm.id == movement.id_fm });
          if(fm){
            switch (fm.id_movement) {
              case 1:
              case 2:
              case 3:
                return ctrl.projects.find(function(project){ return project.id == fm.id_project }).Name;
              case 4:
                return "Capital";
              case 5:
                return ctrl.investors.find(function(investor){ return investor.id == movement.id_investor }).Name;
              default:
              return "error"
            }
          }
        }
        ctrl.setMovementConcept = function(movement){
          var fm = ctrl.fm.find(function(fm){ return fm.id == movement.id_fm });
          if(ctrl.movements.length && fm){
            if(fm.id_movement == 1 || fm.id_movement ==5){
              return ctrl.movements.find(function(mov){ return mov.id == fm.id_movement }).Name
            }else{
              return (ctrl.investors.find(function(investor){ return investor.id == movement.id_investor }).Name +": " + ctrl.movements.find(function(mov){ return mov.id == fm.id_movement }).Name)
            }
          }
        }
        ctrl.getMovementDate = function(movement){
          if(ctrl.fm.length){
            return ctrl.fm.find(function(fm){ return fm.id == movement.id_fm }).Date
          }
        }
        ctrl.getMovementType = function(movement){
          if(ctrl.fm.length){
            return ctrl.fm.find(function(fm){ return fm.id == movement.id_fm }).id_movement
          }
        }
    }
}
export default angular.module('MovementsCtrl', [
    angularMeteor
])
    .component('movements', {
        templateUrl: 'imports/components/movements/movements.html',
        controller: MovementsCtrl
    })
    .config(config);

    function config($stateProvider){
        'ngInject';
        $stateProvider.state('exerciseView.movements',{
            url: '/movimientos',
            template: '<movements></movements>',
            title:'Movimientos',
            cache: false
        });
    }
