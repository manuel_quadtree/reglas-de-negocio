import angular               from 'angular';
import angularMeteor         from 'angular-meteor';
import uiRouter              from 'angular-ui-router';
import exercisesComponent    from '../imports/components/exercises/exercises'
import exerciseViewComponent from '../imports/components/exerciseView/exerciseView'
import navigationComponent   from '../imports/components/navigation/navigation'
import dateTimePicker from 'angular-bootstrap-datetimepicker'
import movementsComponent    from '../imports/components/movements/movements';
import investorsComponent    from '../imports/components/investors/investors';
import projectsComponent     from '../imports/components/projects/projects';
import journalComponent      from '../imports/components/journal/journal';

let app = angular.module('reglas-negocio', [
    angularMeteor,
    uiRouter,
    exercisesComponent.name,
    exerciseViewComponent.name,
    navigationComponent.name,
    movementsComponent.name,
    investorsComponent.name,
    projectsComponent.name,
    journalComponent.name,
    dateTimePicker
]).config(config);


function config($locationProvider, $urlRouterProvider) {
  'ngInject';
  $locationProvider.html5Mode(true);

  $urlRouterProvider.otherwise('/ejercicios');
}
